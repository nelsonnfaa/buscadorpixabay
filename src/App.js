import React, { useState, useEffect } from 'react';
import Formulario from './components/Formulario';
import ListadoImagenes from './components/ListadoImagenes';

function App() {
  const [busqueda, setBusqueda] = useState([]);
  const [terminobusqueda, setTerminobusqueda] = useState('');
  //state para paginacion
  const [paginaactual, setPaginaactual] = useState(1);
  const [totalpaginas, setTotalpaginas] = useState(1);
  useEffect(() => {
    const consultarApi = async () => {
      if (terminobusqueda === '') return;
      const imgporpagina = '30';
      const key = '16176790-39a0ec82c226616964f6f6fac';
      const url = `https://pixabay.com/api/?key=${key}&q=${terminobusqueda}&image_type=photo&per_page=${imgporpagina}&page=${paginaactual}`;
      const respuesta = await fetch(url);
      const resultado = await respuesta.json();
      setBusqueda(resultado.hits);
      setTotalpaginas(Math.ceil(resultado.totalHits / imgporpagina));
      //mover al principio la pagina
      const jumbotron = document.querySelector('.jumbotron');
      jumbotron.scrollIntoView({ behavior: 'smooth' });
    };
    consultarApi();
  }, [terminobusqueda, paginaactual]);

  //pagina anterior
  const paginaAnteriro = () => {
    if (paginaactual === 0) return;
    setPaginaactual(paginaactual - 1);
  };

  //pagina siguiente
  const paginaSiguiente = () => {
    if (paginaactual > totalpaginas) return;

    setPaginaactual(paginaactual + 1);
  };

  return (
    <>
    <div className="container-fluid">
      <div className="row">
        <div className="jumbotron col">
          <p className="lead text-center">Buscador de Imagenes</p>
          <div className="row justify-content-center">
            <Formulario setTerminobusqueda={setTerminobusqueda} />
          </div>
        </div>
      </div>
      </div>
      <div className="container">
      <div className="row justify-content-center mb-4">
        <ListadoImagenes busqueda={busqueda} />
        {paginaactual === 1 ? null : (
          <button
            type="button"
            className="btn btn-info mr-1"
            onClick={paginaAnteriro}
          >
            Anterior &laquo;
          </button>
        )}
        {paginaactual === totalpaginas ? null : (
          <button
            type="button"
            className="btn btn-info"
            onClick={paginaSiguiente}
          >
            Siguiente &raquo;
          </button>
        )}
      </div>
    </div>
    </>
  );
}

export default App;
