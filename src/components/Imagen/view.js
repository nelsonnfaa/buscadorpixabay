import React from 'react';
import PropTypes from 'prop-types';
const Imagen = ({ image }) => {
  const { previewURL, largeImageURL, views, likes, tags } = image;
  return (
    <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
      <div className="card">
        <img src={previewURL} alt={tags} className="card-img-top" />
        <div className="card-body">
          <p className="text-card">Vistas: {views} </p>
          <p className="text-card">likes: {likes} </p>
        </div>
        <div className="card-footer">
          <a
            href={largeImageURL}
            target="_blank"
            rel="noopener noreferrer"
            className="btn btn-primary btn-block"
          >
            Ver Imagen
          </a>
        </div>
      </div>
    </div>
  );
};

Image.propTypes = {
  image: PropTypes.object.isRequired,
};

export default Imagen;
