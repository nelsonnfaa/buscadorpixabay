import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ mensaje }) => {
  return (
    <>
      <p className="my-4 p-4 text-center alert alert-primary">{mensaje}</p>
      <h1>{mensaje}</h1>
    </>
  );
};

Error.propTipes = {
  mensaje: PropTypes.string.isRequired,
};

export default Error;
