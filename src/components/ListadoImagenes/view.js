import React from 'react';
import Imagen from '../Imagen';
import PropTypes from 'prop-types';
const ListadoImagenes = ({ busqueda }) => {
  return (
    <div className="col-12 p-5 row">
      {busqueda.map((image) => (
        <Imagen key={image.id} image={image} />
      ))}
    </div>
  );
};

ListadoImagenes.propTypes = {
  busqueda: PropTypes.object.isRequired,
};

export default ListadoImagenes;
